package hm.edu.cs.se2.medicalapp.medication.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
/** 
* @author Iheb Lachheb
* Class for the data to get from the Prescrible rxNorm
*
*/
@JsonIgnoreProperties(ignoreUnknown = true)
public class Medical {
	
	@JsonProperty("drugGroup")
	private DrugGroup drugGroup;

	public DrugGroup getDrugGroup() {
		return drugGroup;
	}

	public void setDrugGroup(DrugGroup drugGroup) {
		this.drugGroup = drugGroup;
	}
	
	
	

	
	
	
	

}
