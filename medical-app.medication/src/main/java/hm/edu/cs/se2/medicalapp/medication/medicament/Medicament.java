package hm.edu.cs.se2.medicalapp.medication.medicament;


import javax.persistence.Entity;
import javax.persistence.Id;



/**
*
*@author Iheb Lachheb
*
*/
@Entity
public class Medicament {

	

	@Id
	private String name;


	public Medicament() {
		
	}
	
	public Medicament(String name) {
		

		this.name = name;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		
		return this.getName();
	}
	

}
