package hm.edu.cs.se2.medicalapp.medication.treatment;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
/**
 * 
 * @author Iheb Lachheb
 * Crud Repository for treatments
 */
public interface TreatmentRepository extends CrudRepository<Treatment, Integer >{

	List<Treatment> findAllByPatientId(String patientId);

	
	

	
	
	
	

}
