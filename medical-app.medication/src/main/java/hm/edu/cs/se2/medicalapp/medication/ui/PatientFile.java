package hm.edu.cs.se2.medicalapp.medication.ui;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.Notification.Position;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.OptionalParameter;
import com.vaadin.flow.router.Route;

import hm.edu.cs.se2.medicalapp.medication.MainView;
import hm.edu.cs.se2.medicalapp.medication.treatment.Treatment;
import hm.edu.cs.se2.medicalapp.medication.treatment.TreatmentService;

/**
 * 
 * @author Iheb Lachheb 
 * UI shows all treatments of one by Patient by patient-Id
 */
@Route(value = "patientfile", layout = MainView.class)
public class PatientFile extends VerticalLayout implements HasUrlParameter<String> {
	// Attribute
	Grid<Treatment> grid = new Grid<>(Treatment.class);
	@Autowired
	private TreatmentService service;
	private Button deleteButton = new Button("Behandlung löschen");
	private Button anotherMediButton = new Button("Weitere Medikamente hinzufügen");
	private Button backToPatientButton = new Button("Zurück zum Patienten");
	private TextField deleteParameterField = new TextField();
	FormLayout formLayout = new FormLayout();
	private String message;

	public PatientFile(@Autowired TreatmentService service) {

	}

	@Override
	public void setParameter(BeforeEvent event, @OptionalParameter String parameter) {

		List<Treatment> treatment = service.getAllTreatmentsByPatientID(parameter);

// put all Treatment into the grid
		grid.setItems(treatment);
//set columnnames
		grid.setColumns("patientId", "treatmentId", "treatmentCosts", "medicament");

// Field to put the treatment-Id which will be deleted
		deleteParameterField.setLabel("Bitte geben Sie die zu löschende Behandlungs-Id ein");
		deleteParameterField.setValueChangeMode(ValueChangeMode.EAGER);
		formLayout.addComponentAtIndex(0, deleteParameterField);
		formLayout.addComponentAtIndex(1, deleteButton);
// deletes the treatment with the given Treatment-Id
		deleteButton.addClickListener(e -> {
			message = "Behandlung gelöscht";
			try {
				Integer parameterValue = Integer.valueOf(deleteParameterField.getValue());
				deleteTreatment(parameterValue);
				updateList(parameter);
			} catch (NullPointerException e1) {
				message = "Behandlungs-Id existiert nicht";
			} catch (EmptyResultDataAccessException e1) {
				message = "Behandlungs-Id existiert nicht";
			}
			deleteParameterField.clear();
			Notification.show(message, 2000, Position.MIDDLE);
		});

//Button to go back to the Treatmentfile to set another treatment with the given parameter
		anotherMediButton.addClickListener(e -> {
			this.getUI().ifPresent(ui -> ui.navigate("treatment/" + parameter));
			Notification.show("Legen Sie noch ein Medikament an", 2000, Position.MIDDLE);
		});
// Button to get back to the patient service		
		backToPatientButton.addClickListener(e -> {
			UI.getCurrent().getPage()
					.executeJavaScript("window.open(\"http://localhost:8080/patientView/\", \"_self\");");
		});

		updateList(parameter);
		HorizontalLayout buttonLayout = new HorizontalLayout();
		buttonLayout.addComponentAtIndex(0, anotherMediButton);
		buttonLayout.addComponentAtIndex(1, backToPatientButton);

		add(formLayout, grid, buttonLayout);

	}

//Values will be updated after a treatment has been deleted
	public void updateList(String parameter) {
		grid.setItems(service.getAllTreatmentsByPatientID(parameter));
	}

//method to delete a treatment
	public void deleteTreatment(int id) {

		service.deleteTreatment(id);

	}

}