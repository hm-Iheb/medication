package hm.edu.cs.se2.medicalapp.medication.medicament;


import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
/**
 * 
 * @author Iheb Lachheb
 *Crud Repository for medicaments
 */
public interface MedicamentRepository  extends CrudRepository<Medicament, String>{
	
//	public List<Medicament> findByTreatmentId(String treatmentId);
	
	public Optional<Medicament> findByName(String name);
	

}
