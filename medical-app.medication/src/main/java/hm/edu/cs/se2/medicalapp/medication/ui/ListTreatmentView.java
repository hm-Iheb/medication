package hm.edu.cs.se2.medicalapp.medication.ui;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.QueryParameters;
import com.vaadin.flow.router.Route;

import hm.edu.cs.se2.medicalapp.medication.MainView;
import hm.edu.cs.se2.medicalapp.medication.treatment.Treatment;
import hm.edu.cs.se2.medicalapp.medication.treatment.TreatmentService;
/**
 * 
 * @author Iheb Lachheb
 *UI which shows all the treatments in the database
 */
@Route(value = "patientlist", layout = MainView.class)
public class ListTreatmentView extends VerticalLayout {

	public ListTreatmentView(@Autowired TreatmentService service) {

		List<Treatment> treatments = new ArrayList<>();

		treatments.addAll(this.loadTreatments(service));

		Grid<Treatment> grid = new Grid<>(Treatment.class);
		grid.setItems(treatments);
		grid.removeColumnByKey("treatmentId");
		grid.setColumns("patientId","treatmentId", "medicament", "treatmentCosts");
		grid.addComponentColumn(this::createEditButton);
		grid.addComponentColumn(this::deleteButton);

		grid.setWidth("80%");

		add(grid);

		this.setHorizontalComponentAlignment(Alignment.CENTER, grid);
	}
	private Button deleteButton(Treatment treatment) {
		Button button = new Button(new Icon(VaadinIcon.DEL_A));
		return button;
		
	}

	private Button createEditButton(Treatment treatment) {
		Button button = new Button(new Icon(VaadinIcon.EDIT));
		Map<String, String> params = new HashMap<String, String>();
		params.put("id", treatment.getPatientId());
		button.addClickListener(
				e -> this.getUI().ifPresent(ui -> ui.navigate("ui/edit/treatment", QueryParameters.simple(params))));
		return button;
	}

	private List<Treatment> loadTreatments(TreatmentService service) {
		Iterable<Treatment> allTreatments = service.getAllTreatments();
		List<Treatment> treatmentList = new ArrayList<Treatment>();
		allTreatments.forEach(treatmentList::add);
		return treatmentList;
	}
}