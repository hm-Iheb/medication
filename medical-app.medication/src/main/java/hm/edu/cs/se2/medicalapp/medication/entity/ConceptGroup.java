package hm.edu.cs.se2.medicalapp.medication.entity;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * 
 * @author Iheb Lachheb
 * Class for the data to get from the Prescrible rxNorm
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ConceptGroup {
	

	@JsonProperty("conceptProperties")
	private List <ConceptProperties> conceptProperties;

	public List<ConceptProperties> getConceptProperties() {
		return conceptProperties;
	}

	public void setConceptProperties(List<ConceptProperties> conceptProperties) {
		this.conceptProperties = conceptProperties;
	}






//	public ConceptProperties getConceptprop() {
//		return conceptprop;
//	}
//
//	public void setConceptprop(ConceptProperties conceptprop) {
//		this.conceptprop = conceptprop;
//	}

}
