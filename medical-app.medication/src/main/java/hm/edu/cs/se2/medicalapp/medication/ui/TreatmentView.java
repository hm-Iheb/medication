package hm.edu.cs.se2.medicalapp.medication.ui;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.Notification.Position;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.OptionalParameter;
import com.vaadin.flow.router.Route;

import hm.edu.cs.se2.medicalapp.medication.MainView;
import hm.edu.cs.se2.medicalapp.medication.medicament.Medicament;
import hm.edu.cs.se2.medicalapp.medication.medicament.MedicamentService;
import hm.edu.cs.se2.medicalapp.medication.treatment.Treatment;
import hm.edu.cs.se2.medicalapp.medication.treatment.TreatmentService;

/**
 * 
 * @author Iheb Lachheb UI to set a treatment to the by the parameter given
 *         patient and save it to the database
 */
@Route(value = "treatment", layout = MainView.class)
public class TreatmentView extends VerticalLayout implements HasUrlParameter<String> {
// Field of the Page
	TextField patientIdField = new TextField();
	TextField treatmentIdField = new TextField();
	TextField nameField = new TextField();

	NumberField durationField = new NumberField();
	NumberField mg_mlField = new NumberField();
	NumberField costField = new NumberField();
	TextField returnField = new TextField();
	TextField mediField = new TextField();
	ComboBox<Medicament> medBox = new ComboBox<>("Medicaments");
	private String message;

	FormLayout formLayout = new FormLayout();
	Treatment treatment = new Treatment();

	double result;

	@Autowired
	MedicamentService mediService;

	@Autowired
	TreatmentService treatService;
	final Binder<Treatment> binder = new Binder<>();
	Binder<Medicament> medibinder = new Binder<>();
//	private Medicament medicament;
	String mediResult;
	ArrayList<String> searchResult;
	Button searchButton = new Button("Search");
	Button toFileButton = new Button("Zur Behandlungsakte");
	Button backToPatientButton = new Button("Zur�ck zum Patienten");
	List<Medicament> medicaments = new ArrayList<Medicament>();
	List<String> medicamentList = new ArrayList<>();

	public TreatmentView(@Autowired TreatmentService service, @Autowired MedicamentService mediservice) {
	}

//	medicaments.forEach(medicaments::add);
	@Override
	public void setParameter(BeforeEvent event, @OptionalParameter String parameter) {

		medicaments = mediService.getAllMedis();

		medBox.setItems(medicaments);
		medBox.setWidth("20em");

		Button calculateButton = new Button("Kosten berechnen");
// 		shows the current a Patient-Id
		patientIdField.setLabel("PatientID");
		patientIdField.setWidth("20em");
		patientIdField.setValue(parameter);
		patientIdField.setReadOnly(true);
		formLayout.addFormItem(patientIdField, "PatientenId");

		formLayout.addFormItem(medBox, "Medicaments");

//	 	Set the duration of the medication	
		durationField.setWidth("20em");
		durationField.setPlaceholder("Bitte geben Sie die Dauer ein");
		durationField.setValueChangeMode(ValueChangeMode.EAGER);
		formLayout.addFormItem(durationField, "Dauer der Anwendung");

//		Set the Dosage in ml or mg
		mg_mlField.setWidth("20em");
		mg_mlField.setPlaceholder("Bitte geben Sie die Menge an");
		mg_mlField.setValueChangeMode(ValueChangeMode.EAGER);
		formLayout.addFormItem(mg_mlField, "Menge in ml oder mg");

//      A field for the calculated price of the treatment
		costField.setWidth("20em");
		costField.setPlaceholder("Kosten des Medi");
		costField.setValueChangeMode(ValueChangeMode.EAGER);
		costField.setReadOnly(true);
		formLayout.addFormItem(costField, "");

// 		calculates the given parameters
		calculateButton.addClickListener(e -> calculate());

		binder.forField(patientIdField).asRequired("Add Treatment-Id").bind(Treatment::getPatientId,
				Treatment::setPatientId);
		binder.forField(medBox).bind(Treatment::getMedicament, Treatment::setMedicament);
		binder.forField(durationField).bind(Treatment::getTreatmentDuration, Treatment::setTreatmentDuration);
		binder.forField(mg_mlField).bind(Treatment::getDosage, Treatment::setDosage);
		binder.forField(costField).bind(Treatment::getTreatmentCosts, Treatment::setTreatmentCosts);

//		Button to save the data by binding the Attributes
		Button saveButton = new Button("Save");
		add(saveButton);
		saveButton.addClickListener(e -> {
			try {
				if (binder.writeBeanIfValid(treatment)) {
					binder.setBean(treatment);
					treatService.addTreatment(treatment);
					binder.readBean(null);
					message = "Behandlung gespeichert!";
				}
			} catch (NullPointerException e1) {
				message = "Behandlung konnte wegen fehlender Kostenkalkulation nicht gespeichert werden!";

			}
			this.getUI().ifPresent(ui -> ui.navigate("patientfile/" + parameter));

			this.getUI().ifPresent(ui -> ui.navigate("patientfile/" + parameter));
			Notification.show(message, 3000, Position.MIDDLE);
		});

		toFileButton.addClickListener(e -> {
			this.getUI().ifPresent(ui -> ui.navigate("patientfile/" + parameter));
			Notification.show("Behandlungakte", 2000, Position.MIDDLE);
		});

// Button to get back to the patient service		
		backToPatientButton.addClickListener(e -> {
			UI.getCurrent().getPage()
					.executeJavaScript("window.open(\"http://localhost:8080/patientView/\", \"_self\");");
		});

		formLayout.setResponsiveSteps(new FormLayout.ResponsiveStep("21em", 1));
		HorizontalLayout buttonLayout = new HorizontalLayout();

		buttonLayout.addComponentAtIndex(0, calculateButton);
		buttonLayout.addComponentAtIndex(1, saveButton);
		buttonLayout.addComponentAtIndex(2, toFileButton);
		buttonLayout.addComponentAtIndex(3, backToPatientButton);

		add(formLayout, buttonLayout);

	}

	private void calculate() throws NullPointerException {
		message = "Medikationskosten berechnet";
		try {

			costField.setValue(treatService.calculate(durationField.getValue(), mg_mlField.getValue()));

		} catch (NullPointerException e) {
			message = "Bitte �berpr�fen Sie nochmal Ihre Eingaben";
		}
		Notification.show(message, 3000, Position.MIDDLE);

	}

}
