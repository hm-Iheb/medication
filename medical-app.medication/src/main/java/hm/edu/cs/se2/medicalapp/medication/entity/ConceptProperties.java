package hm.edu.cs.se2.medicalapp.medication.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
/** 
* @author Iheb Lachheb
* Class for the data to get from the Prescrible rxNorm
*
*/
@JsonIgnoreProperties(ignoreUnknown = true)
public class ConceptProperties {
	
	

	@JsonProperty("name")
	private String name;
	@JsonProperty("rxcui")
	private String rxcui;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRxcui() {
		return rxcui;
	}

	public void setRxcui(String rxcui) {
		this.rxcui = rxcui;
	}

}
