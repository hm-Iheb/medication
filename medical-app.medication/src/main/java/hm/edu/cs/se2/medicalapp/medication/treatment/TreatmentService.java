package hm.edu.cs.se2.medicalapp.medication.treatment;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
/**
 * 
 * @author Iheb Lachheb
 *Service class to  get, save, update, delete treatments
 */
@Service
public class TreatmentService {
	
	private double result;

	@Autowired
	private TreatmentRepository treatRepository;
// 	method to get all treatments in the database
	public List<Treatment> getAllTreatments() {
		List<Treatment> treatments = new ArrayList<>();
		treatRepository.findAll().forEach(treatments::add);
		return treatments;
	}
// 	method to get one treatment by the treatment-id
	public Optional<Treatment> getTreatment(int id) {
		return treatRepository.findById(id);
	}
//	method to get all treatments by patient-id
	public List<Treatment> getAllTreatmentsByPatientID(String patientId) {

		return treatRepository.findAllByPatientId(patientId);

	}

// 	method to to save the treatment
	public void addTreatment(Treatment treatment) {
		treatRepository.save(treatment);

	}
//	methode to update one treatment by id
	public void updateTreatment(String id, Treatment treatment) {
		treatRepository.save(treatment);

	}
//	method to delete one treatment by id
	public void deleteTreatment(int id) {
		treatRepository.deleteById(id);
	}
	
	public double calculate(double duration, double dosage) throws NullPointerException {
		if(duration <= 0 || dosage <= 0) {
			throw new NullPointerException();
		}
		if (duration > 19)
			result = 5 * dosage;
		else if (duration > 9)
			result = 10 * dosage;
		else
			result = 15 * dosage;
		return result;
		}
	
	

}
