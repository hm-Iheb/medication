package hm.edu.cs.se2.medicalapp.medication.entity;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
/** 
* @author Iheb Lachheb
* Class for the data to get from the Prescrible rxNorm
*
*/
@JsonIgnoreProperties(ignoreUnknown = true)
public class DrugGroup {

	@JsonProperty("name")
	private String name;

	@JsonProperty("conceptGroup")
	private List<ConceptGroup> conceptgroup;

	

	public List<ConceptGroup> getConceptgroup() {
		return conceptgroup;
	}

	public void setConceptgroup(List<ConceptGroup> conceptgroup) {
		this.conceptgroup = conceptgroup;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
