package hm.edu.cs.se2.medicalapp.medication.treatment;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TreatmentController {

	@Autowired
	private TreatmentService treatmentService;

	@RequestMapping("/treatments")
	public List<Treatment> getAllTreatments() {
		return treatmentService.getAllTreatments();

	}

	@RequestMapping("treatments/{id}")
	public Optional<Treatment> getTreatment(@PathVariable int id) {
		return treatmentService.getTreatment(id);

	}

	@RequestMapping(method = RequestMethod.POST, value = "/treatments")
	public void addTreatment(@RequestBody Treatment treatment) {
		treatmentService.addTreatment(treatment);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/treatments/{id}")
	public void updateTreatment(@RequestBody Treatment treatment, @PathVariable String id) {
		treatmentService.updateTreatment(id, treatment);

	}

//	@RequestMapping(method = RequestMethod.DELETE, value = "treatments/{id}")
//	public void deleteTreatment(@PathVariable String id) {
//		treatmentService.deleteTreatment(id);
//	}

}
