package hm.edu.cs.se2.medicalapp.medication.treatment;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;


import hm.edu.cs.se2.medicalapp.medication.medicament.Medicament;
/**
 * 
 * @author Iheb Lachheb
 *Treatment class
 */
@Entity
public class Treatment {
	
// Attributs	
	private String patientId;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int treatmentId;
//	private String mediName;
	@ManyToOne
	private Medicament medicament;
	private double treatmentDuration;

	private double treatmentCosts;

	private double dosage;
//	@Id
//	private String patientId;

	public Treatment() {

	}

	public Treatment(int id, Medicament medicament, double treatmentDuration, double treatmentCosts, double dosage,
			String patientId) {
		super();
		this.treatmentId= id;
		this.medicament = medicament;
		this.treatmentDuration = treatmentDuration;
		this.treatmentCosts = treatmentCosts;
		this.dosage = dosage;
		this.patientId = patientId;
	}

	public double getTreatmentCosts() {
		return treatmentCosts;
	}

	public int getTreatmentId() {
		return treatmentId;
	}

	public void setTreatmentId(int treatmentId) {
		this.treatmentId = treatmentId;
	}

	public void setTreatmentCosts(double treatmentCosts) {
		this.treatmentCosts = treatmentCosts;
	}

	public void setTreatmentDuration(double treatmentDuration) {
		this.treatmentDuration = treatmentDuration;
	}

	public void setDosage(double dosage) {
		this.dosage = dosage;
	}


	public double getDosage() {
		return dosage;
	}

	public void setDosage(int dosage) {
		this.dosage = dosage;
	}

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String id) {
		this.patientId = id;
	}

	public double getTreatmentDuration() {
		return treatmentDuration;
	}

	public void setTreatmentDuration(int treatmentDuration) {
		this.treatmentDuration = treatmentDuration;
	}

	public Medicament getMedicament() {
		return medicament;
	}

	public void setMedicament(Medicament medicament) {
		this.medicament = medicament;
	}

}
