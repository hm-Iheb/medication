package hm.edu.cs.se2.medicalapp.medication.medicament;

import java.net.UnknownHostException;

import java.util.ArrayList;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import hm.edu.cs.se2.medicalapp.medication.entity.Medical;

/**
 * 
 * @author Iheb Lachheb
 *Service class to  get, save, update, delete medicaments
 */
@Service
public class MedicamentService {

	@Autowired
	private MedicamentRepository mediRepository;
	private int resultSize;

// 	method to get all medicaments
	public List<Medicament> getAllMedis() {
		List<Medicament> medicaments = new ArrayList<>();
		mediRepository.findAll().forEach(medicaments::add);
		return medicaments;
	}
//	method to get one medicament by one id
	public Optional<Medicament> getMedicament(String name) {
//	 return	medicaments.stream().filter(m -> m.getId().equals(id)).findFirst().get();
		return mediRepository.findByName(name);
	}

// 	method to save one medicament
	public void addMedicament(Medicament medicament) {
		mediRepository.save(medicament);

	}
//	method to update one medicament
	public void updateMedicament(Medicament medicament) {
		mediRepository.save(medicament);

	}
// 	method to delete one medicament
	public void deleteMedicament(String id) {
		mediRepository.deleteById(id);
	}
//	method to get medicament from an extern database
	public ArrayList<String> searchMediName(String name) throws UnknownHostException, NullPointerException {

		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		String resourceURL = "https://rxnav.nlm.nih.gov/REST/Prescribe/drugs.json?name=";
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		ResponseEntity<Medical> response = restTemplate.exchange(resourceURL + name, HttpMethod.GET, entity,
				Medical.class);
		ArrayList<String> results = new ArrayList<>();

		resultSize = response.getBody().getDrugGroup().getConceptgroup().get(1).getConceptProperties().size();

		for (int i = 0; i < resultSize; i++) {
			results.add(i,
					response.getBody().getDrugGroup().getConceptgroup().get(1).getConceptProperties().get(i).getName());

		}

		return results;

	}
}
