package hm.edu.cs.se2.medicalapp.medication.medicament;




import java.net.UnknownHostException;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;



@RestController
public class MedicamentController {
	
	@Autowired
	private MedicamentService medicamentService;
	
//	@RequestMapping("/treatments/{id}/medicaments")
//	public List <Medicament> getAllMedis(@PathVariable String id){
//		return medicamentService.getAllMedis(id);
//						
//			
//	}
	@RequestMapping("/treatments/{treatmentId}/medicaments/{id}")
	public Optional<Medicament> getMedicament(@PathVariable String id) {
		return medicamentService.getMedicament(id);
		
	}
	@RequestMapping(method=RequestMethod.POST, value="/treatments/{treatmentId}/medicaments")
	public void addMedicament(@RequestBody Medicament medicament, @PathVariable String treatmentId) {
//		medicament.setTreatment(new Treatment(treatmentId, 0, 0, null));
		medicamentService.addMedicament(medicament);
	}
	
	
	@RequestMapping(method=RequestMethod.PUT, value="/treatments/{treatmentId}/medicaments/{id}")
	public void updateMedicament(@RequestBody Medicament medicament,@PathVariable String treatmentId,@PathVariable String id) {
//		medicament.setTreatment(new Treatment(treatmentId, 0, 0, null));
		medicamentService.updateMedicament(medicament); 
	
	
	}
	@RequestMapping(method= RequestMethod.DELETE, value="/treatments/{treatmentId}/medicaments/{id}")
		public void deleteMedicament(@PathVariable String id) {
			medicamentService.deleteMedicament(id);
	}
	
	
	
	@RequestMapping(value = "/search/{name}",method = RequestMethod.GET)
	public ArrayList<String> searchMediName(@PathVariable String name) throws UnknownHostException, NullPointerException {
		return medicamentService.searchMediName(name);
		
	}

//	@RequestMapping(method = RequestMethod.POST, value = "/search/{name}/save")
//	public void addmediSave(@RequestBody Medicament medicament, @PathVariable String name) {
//		medicamentService.saveToBaseMedicament(name, medicament);
//		
//
//	}

}
