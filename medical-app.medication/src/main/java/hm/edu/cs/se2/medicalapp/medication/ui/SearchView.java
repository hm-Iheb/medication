package hm.edu.cs.se2.medicalapp.medication.ui;

import java.net.UnknownHostException;

import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.listbox.ListBox;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.Notification.Position;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.Route;

import hm.edu.cs.se2.medicalapp.medication.MainView;
import hm.edu.cs.se2.medicalapp.medication.medicament.MedicamentService;
import hm.edu.cs.se2.medicalapp.medication.medicament.Medicament;
/**
 * 
 * @author Iheb Lachheb
 * UI to search for medicaments and save them to the database
 *
 */
@Route(value = "medsearch", layout = MainView.class)
public class SearchView extends VerticalLayout {

	final Binder<Medicament> binder = new Binder<>();
// 	Fields and Buttons
	Medicament medicament = new Medicament();
	TextField mediField = new TextField();
	TextField returnField = new TextField();
	Button searchButton = new Button("Suchen");
	Button backToPatientButton = new Button("Zur�ck zu den Patienten");
	String result;
	ArrayList<String> searchResult;
	FormLayout formLayout = new FormLayout();
	TextField returnFieldTwo = new TextField();
	ListBox<String> mediBox = new ListBox<>();

	@Autowired
	MedicamentService mediService;

	public SearchView(@Autowired MedicamentService service) {
		setDefaultHorizontalComponentAlignment(Alignment.CENTER);

		mediField.setPlaceholder("Bitte geben Sie einen Namen an");
		mediField.setWidth("30em");
		formLayout.addFormItem(mediField, "Suche");

		formLayout.addFormItem(returnField, "Suchergebnis");

		H1 header = new H1("Hello Doc");

		header.getElement().getThemeList().add("dark");
//		by pushing the Button the search function will be triggered
		searchButton.addClickListener(e -> {
			try {
				nameSearcher();
			} catch (NullPointerException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (UnknownHostException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		});

		binder.forField(mediBox).bind(Medicament::getName, Medicament::setName);
// 		Saves the found medicament
		Button saveButton = new Button("Save");
		add(saveButton);
		saveButton.addClickListener(event -> {

			if (binder.writeBeanIfValid(medicament)) {
				binder.setBean(medicament);
				service.addMedicament(medicament);
				binder.readBean(null);

			}
		});
		backToPatientButton.addClickListener(e -> {
			UI.getCurrent().getPage().executeJavaScript("window.open(\"http://localhost:8080/SearchPatient/\", \"_self\");");

			});
		HorizontalLayout buttonLayout = new HorizontalLayout();
		buttonLayout.addComponentAtIndex(0, searchButton);
		buttonLayout.addComponentAtIndex(1, saveButton);
		buttonLayout.addComponentAtIndex(2, backToPatientButton);
		add(header, mediField, mediBox, buttonLayout);

	}
// 		Uses the function of the service to search for medicalnames	
	public void nameSearcher() throws UnknownHostException {

		result = mediField.getValue();
		mediField.clear();
		String message = "Medikament gefunden";
		try {
			searchResult = mediService.searchMediName(result);
		} catch (UnknownHostException e) {
			message = "Leider existiert dieses Medikament nicht!";
		} catch (NullPointerException e) {
			message = "Leider existiert dieses Medikament nicht!";
		}
		Notification.show(message, 2000, Position.MIDDLE);

		mediBox.setItems(searchResult);

	}

}
