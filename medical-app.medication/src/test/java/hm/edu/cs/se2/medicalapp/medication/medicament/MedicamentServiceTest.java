package hm.edu.cs.se2.medicalapp.medication.medicament;

import static org.junit.Assert.assertEquals;


import java.net.UnknownHostException;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MedicamentServiceTest {
	@Autowired
	private MedicamentService mediService;
	String expectedMediName = "atorvastatin 80 MG Oral Tablet [Lipitor]";
	Medicament mediOne = new Medicament("lipitor");
// Tests whether we get expected Medicament form the API
	@Test
	public void testSearchMedis() throws UnknownHostException, NullPointerException {
		String foundMedi = mediService.searchMediName("lipitor").get(0);
		assertEquals(expectedMediName, foundMedi);
	}
// Tests the case if we search for non exsisting medicament
	@Test(expected = NullPointerException.class)
	public void testSearchMedisTwo() throws UnknownHostException, NullPointerException {
		String foundMedi = mediService.searchMediName("lipi").get(0);

	}
}
