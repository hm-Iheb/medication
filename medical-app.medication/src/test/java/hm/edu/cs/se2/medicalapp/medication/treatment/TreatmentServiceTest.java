package hm.edu.cs.se2.medicalapp.medication.treatment;

import static org.junit.Assert.assertEquals;

import java.net.UnknownHostException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TreatmentServiceTest {

	@Autowired
	private TreatmentService treatService;
// Tests the usage of null or negativ values
	@Test(expected = NullPointerException.class)
	public void calculateTest() throws UnknownHostException, NullPointerException {
		treatService.calculate(0, -1);

	}
// first equivalence class duration over 19 days
	@Test
	public void calculateTestTwo() {
		double actual = treatService.calculate(20, 1);
		assertEquals(5.0, actual, 0.000001);
	}
// second equivalence class duration over 9 days
	@Test
	public void calculateTestThree() {
		double actual = treatService.calculate(12, 1);
		assertEquals(10.0, actual, 0.000001);
	}
//  third equivalence class duration under 9 days
	@Test
	public void calculateTestFour() {
		double actual = treatService.calculate(1, 1);
		assertEquals(15.0, actual, 0.000001);
	}
}